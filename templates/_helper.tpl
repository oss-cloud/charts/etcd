{{- define "scheme" -}}
{{- if .Values.tls.client.enabled -}}
https
{{- else -}}
http
{{- end -}}
{{- end -}}

{{- define "domain" -}}
{{ .Release.Name }}-etcd.{{ .Release.Namespace }}.svc.{{ .Values.clusterDomain }}
{{- end -}}

{{- define "url.peer" -}}
{{- template "scheme" -}}://$HOSTNAME.{{ template "domain" }}:2380
{{- end -}}

{{- define "url.client" -}}
{{- template "scheme" -}}://$HOSTNAME.{{ template "domain" }}:2379
{{- end -}}

{{- define "selector" -}}
{{- $name := default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
helm.sh/release: {{ .Release.Name }}
app.kubernetes.io/name: {{ $name | quote }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "labels" -}}
{{- $name := default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
helm.sh/release: {{ .Release.Name }}
app.kubernetes.io/name: {{ $name | quote }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}
